from django.apps import AppConfig


class RecipeeapiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "recipeeApi"
